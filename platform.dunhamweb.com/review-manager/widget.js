(function() {
  //Load Resources Into Header
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  var link = document.createElement('link');
  link.rel = 'stylesheet';
  script.src = 'https://code.jquery.com/jquery-3.3.1.min.js';
  link.href = 'https://platform.dunhamweb.com/review-manager/css/widget.css';
	head.appendChild(link);

	var i;
	var s;
	var starState;
  var wg_link_target = '';

  // Get Reviews Button data
  var wg_data_target = document.getElementsByClassName('tdg-reviews-badge')[0];
	if (wg_data_target !== undefined){
		//var wg_client_id = 'client=' + wg_data_target.getAttribute("data-client");
		var wg_client_id = wg_data_target.getAttribute("data-client");
    var wg_pid = wg_data_target.getAttribute("data-location");
		//var wg_pos = wg_data_target.getAttribute("data-position");
		var wg_url = wg_data_target.getAttribute("data-url");
		if (wg_url === null) {
			wg_url = '/reviews';
		}else if(wg_url.search('search.google')){
      wg_link_target = ' target="_blank"';
    }
	}

	// Get Reviews List container data
	// get client id: keep in container widget for scenarios where client wants footer widget removed from the page
	var pg_data_target = document.getElementsByClassName('tdg-reviews')[0];
	if (pg_data_target !== undefined){
		//var client_id = pg_data_target.getAttribute("data-client");
		//var pg_client_id = 'client=' + pg_data_target.getAttribute("data-client");
		var pg_client_id = pg_data_target.getAttribute("data-client");
		var pg_pid = pg_data_target.getAttribute("data-location");
	}

  // Check if reviews container exists
  function reviewsContainerExists() {
    return document.getElementsByClassName('tdg-reviews').length;
  }

  //Load reviews container
  if (reviewsContainerExists()) {

		var list_endpoint_url = 'https://platform.dunhamweb.com/review-manager/ajax/review-container-controller.php?client='+pg_client_id+'&pid='+pg_pid+'&callback=';

    // Non-WordPress Handler
    function reviewListHelper(){
			$(document).ready(function(){
				$.getJSON(list_endpoint_url, function(res) {
					$('.tdg-reviews').html(outputWriter(res));
				});
			});
    }

    // WordPress Handler
    function reviewListHelperWP(){
      jQuery(document).ready(function(){
        jQuery.getJSON(list_endpoint_url, function(res) {
					jQuery('.tdg-reviews').html(outputWriter(res));
        });
      });
    }

    // Insert Data Into Review Containers
    function outputWriter(res){
      var rating = Math.round(res.avg_rating * 2) / 2;
      var output = '<a href="https://search.google.com/local/writereview?placeid='+res.place_id+'" target="_blank" class="btn">Write a Review</a><div role="link" class="aggregate-rating"><div class="tdg-reviews-rating">';
			for(i = 1; i <= 5; i++){
				if(i < rating || i == rating){
					starState = 'on';
				} else if((i - 0.6 >= rating) && (i - 0.3 < (rating) + 3)){
					starState = '';
				} else {
					starState = 'half';
				}
				output += '<span class="star '+starState+'"></span>';
			}
			output += '<div class="aggregate-rating-value">'+parseFloat(res.avg_rating).toFixed(1)+'</div></div></div><div>Based on '+res.review_array.length+' Google Reviews</div><hr><div>';
			for(i = 0; i < res.review_array.length; i++){
				// individual review container
				output += '<div><div class="tdg-reviews-rating">';
				// loop and display the stars
				for(s = 1; s <= 5; s++){
					if(s < res.review_array[i].rating || s == res.review_array[i].rating){
						starState = 'on';
					} else if((s - 0.6 >= res.review_array[i].rating) && (s - 0.3 < (res.review_array[i].rating) + 3)) {
						starState = '';
					} else {
						starState = 'half';
					}
					output += '<span class="star '+starState+'"></span>';
				}
				output += '</div>';
				// diplay review details
				output += '<div class="review-name">'+res.review_array[i].name+'</div><div class="review-date">'+res.review_array[i].time.replace('th','')+'</div><p>';
				if(res.review_array[i].body){
					output += res.review_array[i].body;
				} else {
					output += '<em>(This customer did not leave any comments)</em>';
				}
				output += '</p></div>';
				// add line on all entries except last
				if(i != res.review_array.length-1){
					output += '<hr>';
				}
			}
      output += '</div>';
      return output;
    }
  }

	var button_endpoint_url = 'https://platform.dunhamweb.com/review-manager/ajax/review-container-controller.php?client='+wg_client_id+'&pid='+wg_pid+'&callback=';

  // Widget Loader - All Sites
  function badgeHelper(){
		$(document).ready(function(){
			$.getJSON(button_endpoint_url, function(res){
				$('.tdg-reviews-badge').html(wg_outputWriter(res));
			});
		})( jQuery );
  }

  // Widget Loader - WordPress Sites
  function badgeHelperWP(){
    jQuery(document).ready(function(){
			jQuery.getJSON(button_endpoint_url, function(res){
        jQuery('.tdg-reviews-badge').html(wg_outputWriter(res));
      });
    });
  }

  // Insert Data Into Widgets //
  function wg_outputWriter(res){
    var rating = Math.round(res.avg_rating * 2) / 2;
    var output = '<a href="'+wg_url+'"'+wg_link_target+'><div class="logo"><img src="https://platform.dunhamweb.com/review-manager/images/google-g.svg" alt="Google"></div><div class="text"><span>Read our <strong>Google</strong> Reviews</span><div class="tdg-reviews-rating">';
		for(i = 1; i <= 5; i++) {
			if(i < rating || i == rating){
				starState = 'on';
			} else if((i - 0.6 >= rating) && (i - 0.3 < (rating) + 3)){
				starState = '';
			}else {
				starState = 'half';
			}
			output += '<span class="star '+starState+'"></span>';
		}
		output += '</div></div></a>';
    return output;
  }

  // Jquery Loader
  if(typeof jQuery == 'undefined'){
    function getScript(url, success) {
      var script = document.createElement('script');
      script.src = url;

      var head = document.getElementsByTagName('head')[0];
      var done = false;

      // Attach handlers for all browsers
      script.onload = script.onreadystatechange = function() {
        if(!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')){
        	done = true;
          // callback function provided as param
          success();
          script.onload = script.onreadystatechange = null;
          head.removeChild(script);
        }
      }
      head.appendChild(script);
    }
    getScript('https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js', function(){
			reviewsContainerExists() ? reviewListHelper() : null;
			badgeHelper();
    });
  } else { // jQuery was already loaded
    reviewsContainerExists() ? reviewListHelperWP() : null
    badgeHelperWP();
  }
})();
