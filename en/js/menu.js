$('.main-menu > li > a').click(function(e){
  if ($(this).parent().has('ul')){
    if ($(window).width() <= 768 && $(this).next().children().length > 0){
      //$(this).next().toggle();/*this line is used in the mobile nav*/
      $(this).next().toggleClass("show");
      e.preventDefault();
    }
  }
});
//prepend menu icon
$('nav').prepend('<button id="menu-mobile-icon" class="lg-hide"><i class="fa fa-bars"></i> Menu</button>');
//toggle nav
$("#menu-mobile-icon").on("click", function(){
  $(".main-menu").toggleClass("show");
});
//Make the navigation accessible
$('.main-menu > li a').focus(function(){
  $(this).parent().parent().children().removeClass('hover');
  $(this).parent().toggleClass('hover');
});